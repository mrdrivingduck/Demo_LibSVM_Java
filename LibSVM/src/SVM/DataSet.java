package SVM;

/**
 * @since 2018.6.18
 * @author Mr Dk.
 * @see http://archive.ics.uci.edu/ml/datasets/MONK%27s+Problems
 */

public class DataSet {
	
	public static String[] monk_1_train_argv = {
			"data/monk-1-train.txt",		// Training Set
			"data/monk-1-model.txt"		// Training model output
	};
	
	public static String[] monk_2_train_argv = {
	        "-s", "0",
            "-c", "32768.0",
            "-g", "0.03125",
			"data/monk-2-train.txt",		// Training Set
			"data/monk-2-model.txt"     // Training model output
	};
	
	public static String[] monk_3_train_argv = {
			"-s", "0",
			"-t", "2",
			"-c", "32.0",
			"-g", "0.03125",
			"data/monk-3-train.txt",		// Training Set
			"data/monk-3-model.txt"		// Training model output
	};

	public static String[] monk_1_test_argv = {
			"data/monk-1-test.txt",		// Test Set
			"data/monk-1-model.txt",		// Model
			"data/monk-1-result.txt"		// Result
	};
	
	public static String[] monk_2_test_argv = {
			"data/monk-2-test.txt",		// Test Set
			"data/monk-2-model.txt",		// Model
			"data/monk-2-result.txt"		// Result
	};
	
	public static String[] monk_3_test_argv = {
			"data/monk-3-test.txt",		// Test Set
			"data/monk-3-model.txt",		// Model
			"data/monk-3-result.txt"		// Result
	};
}
