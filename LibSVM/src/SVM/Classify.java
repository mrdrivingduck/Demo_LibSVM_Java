package SVM;

import java.io.IOException;

/**
 * @since 2018.6.18
 * @author Mr Dk.
 * 
 */

/*
 * 
 * Data set format:
 * 
 *     TAG index:value index:value index:value ......
 *     
 */

public class Classify {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.out.println("SVM Start");
		long start = System.currentTimeMillis();
		
		// Train
		svm_train.main(DataSet.monk_3_train_argv);
		System.out.println("Training time:" + (System.currentTimeMillis() - start) + "ms");
		
		// Predict
		svm_predict.main(DataSet.monk_3_test_argv);
	}

}
